﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    [System.Serializable]
    public class EnemeyModel
    {
        public int health = 0;
        public Vector2 referncePosition;
        public float timeToAttack = 10;
    }
    [SerializeField]
    public EnemeyModel enemyModel;
    public enum EnemyType { A, B, C }
    public EnemyType enemyType;

    public float speed = 1;
    public float rotateSpeed = 1;
    public List<int> destroySequenceList;
    public List<int> currentSequenceList;

    public int predictedInt = 0;
    private int currentIndex = 0;

    public GameObject[] onHitEffect;

    public GameObject[] onDestroyEffect;
    public AudioClip onDestroySound, onHitSound;
    #region other component
    public AudioSource audioSource;
    public GameManager gameManager;

    public Color objectColor;
    public float colorDecreaseStep;
    public GameObject humanGameObject;
    public bool attack;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        audioSource = gameManager.GetComponent<AudioSource>();
        predictedInt = 0;
        objectColor = GetComponent<SpriteRenderer>().color;
        humanGameObject = GameObject.FindGameObjectWithTag("Human");

        switch (enemyType)
        {

            case EnemyType.A:
                destroySequenceList = new List<int> { 0, 1, 0 };
                break;
            case EnemyType.B:
                destroySequenceList = new List<int> { 0, 1, 0, 1, 0 };
                break;
            case EnemyType.C:
                destroySequenceList = new List<int> { 0, 1, 0, 1, 0, 1 };
                break;
            default:
                destroySequenceList = new List<int> { 0 };
                break;
        }
        colorDecreaseStep = objectColor.a / destroySequenceList.Count;

        Invoke("SetAttack", enemyModel.timeToAttack);
    }

    // Update is called once per frame
    void Update()
    {
        switch (enemyType)
        {
            case EnemyType.A:
                Enemy_A_Behav();
                break;
            case EnemyType.B:
                Enemy_B_Behav();
                break;
            case EnemyType.C:
                Enemy_C_Behave();
                break;
            default:
                break;
        }
       // AttackHuman();
        MoveToTarget();
        Animate();
    }

    public void Enemy_A_Behav()
    {
        if (currentSequenceList.Count == destroySequenceList.Count)
        {
            if (currentSequenceList == destroySequenceList)
            {
                Destroy(this.gameObject);
            }
        }

    }

    public void OnHit_A_Behave(int hitOb)
    {

        if (hitOb == predictedInt)
        {
            currentSequenceList.Add(hitOb);
            currentIndex += 1;
            predictedInt = destroySequenceList[currentIndex];

        }
        else
        {
            currentSequenceList.Clear();
        }
    }


    public void Enemy_B_Behav()
    {

    }
    public void OnHit_B_Behave(int hitOb)
    {
        if (hitOb == predictedInt)
        {
            currentSequenceList.Add(hitOb);
            currentIndex += 1;
            predictedInt = destroySequenceList[currentIndex];

        }
        else
        {
            Debug.Log("ERROR");
        }
    }


    public void Enemy_C_Behave()
    {

    }

    public void OnHit_C_Behave(int hitOb)
    {

    }

    public void OnHit(string obTag)
    {
        int typeId = 0;
        if (obTag == "soap")
        {
            typeId = 1;
        }
        if (obTag == "water")
        {
            typeId = 0;
        }
        OnHitCommonBehave(typeId);
        Debug.Log("Hitted");
        switch (enemyType)
        {
            case EnemyType.A:
                //    OnHit_A_Behave(typeId);
                break;
            case EnemyType.B:
                //  OnHit_B_Behave(typeId);
                break;
            case EnemyType.C:
                //  OnHit_C_Behave(typeId);
                break;
            default:
                break;
        }
    }
    public void OnHitCommonBehave(int hitOb)
    {

      //  Vibration.Vibrate(3);
        if (hitOb == predictedInt)
        {
            currentSequenceList.Add(hitOb);
            if (destroySequenceList.Count == currentSequenceList.Count)
            {
                Debug.Log("HavetoDestoy");
                Instantiate(onDestroyEffect[0], transform.position, Quaternion.identity);
                audioSource.PlayOneShot(onDestroySound);
                Destroy(this.gameObject);
                return;
            }
            else
            {
                currentIndex += 1;
                predictedInt = destroySequenceList[currentIndex];
                audioSource.PlayOneShot(onHitSound);
                objectColor.a -= colorDecreaseStep;
                GetComponent<SpriteRenderer>().color = objectColor;

            }


        }
        else
        {
            currentIndex = 0;
            currentSequenceList.Clear();
            predictedInt = destroySequenceList[0];
            objectColor.a = 1;
            GetComponent<SpriteRenderer>().color = objectColor;

        }
        Instantiate(onHitEffect[0], transform.position, Quaternion.identity);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Human"))
        {
            OnVirusHitHuman();
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("soap") || collision.gameObject.CompareTag("water"))
        {
            if (collision.gameObject.GetComponent<ObjectBehaviour>().canHit)
            {
                Debug.Log("ONHITT");
                OnHit(collision.gameObject.tag);
                Destroy(collision.gameObject);
            }
        }
    }
    private void OnDestroy()
    {
       // GridGenerator gridGen = FindObjectOfType<GridGenerator>();
      //  if(gridGen)
//gridGen.ClearPositionState(enemyModel.referncePosition);
    }
    public void AutoDestroy(float time)
    {
        Destroy(this.gameObject, time);
    }
    public void SetAttack()
    {
        attack = true;
        GetComponent<Animator>().SetTrigger("Attack");
    }
    public void AttackHuman()
    {
        if (attack)
        {
            transform.position = Vector3.Lerp(transform.position, humanGameObject.transform.position, Time.deltaTime);
        }
    }
    private bool canAnimate;
    public void Animate()
    {
       // if (canAnimate)
        {
            transform.Rotate(0,0,Time.deltaTime*rotateSpeed);
        }
    }
    public void MoveToTarget()
    {
        transform.Translate(0, -Time.deltaTime*speed, 0,Space.World);
    }
    public void OnVirusHitHuman()
    {
        Instantiate(onHitEffect[1], transform.position, Quaternion.identity);
        Instantiate(onHitEffect[2], transform.position, Quaternion.identity);

        Destroy(this.gameObject);
    }
}
