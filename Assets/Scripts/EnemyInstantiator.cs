﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(GridGenerator))]

public class EnemyInstantiator : MonoBehaviour
{
   
    public float timeInstantiate = 3;
    public GameObject currentVirus;
    public List<GameObject> allvirusesList;
    public GameManager gameManager;
    public GridGenerator gridGenerator;

    public bool startGame;
    // Start is called before the first frame update
    void Start()
    {
        gridGenerator = gameManager.GetComponent<GridGenerator>();
        //   InvokeRepeating("InstantiateEnemy", 1,timeInstantiate);
        InvokeRepeating("InstantiateEnemyUp", 1, timeInstantiate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void InstantiateEnemy()
    {
        if (!startGame) { return; }
        var enemyPos = gridGenerator.GetRandomPosition();
        GameObject ob = Instantiate(allvirusesList[Random.Range(0,allvirusesList.Count)],enemyPos[0], Quaternion.identity);
        ob.GetComponent<EnemyBehaviour>().enemyModel.referncePosition = enemyPos[1];
    }
    public void InstantiateEnemyUp()
    {
        if (startGame)
        {
            GameObject ob = Instantiate(allvirusesList[Random.Range(0, allvirusesList.Count)], transform.position, Quaternion.identity);
        }
    }
    public void OnStartGame()
    {
        startGame = true;
    }
    public void OnEndGame()
    {
        startGame = false;
    }
    public void OnPauseGame()
    {

    }
   
}
