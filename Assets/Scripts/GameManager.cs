﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameObject inGameObjects;
    public EnemyInstantiator enemyInstantiator;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        enemyInstantiator = GetComponentInChildren<EnemyInstantiator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowPage(int id)
    {
        FindObjectOfType<Menu>().ShowPage(id);
        if (id != 2)
            inGameObjects.SetActive(false);
        else
        {
            inGameObjects.SetActive(true);
            StartGame();
        }
    }
   public void StartGame()
    {
        enemyInstantiator.enabled = true;
        enemyInstantiator.OnStartGame();

    }
    public void PauseGame()
    {

    }
    public void EndGame()
    {
        enemyInstantiator.enabled = false;

    }
}
