﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameViewManager : MonoBehaviour
{
    public Button mainMenuStartGameButton, nameInputStartGameButton;
    // Start is called before the first frame update
    void Start()
    {
        //mainMenuStartGameButton = GameObject.Find("mainMenuStartGameButton").GetComponent<Button>();
        //nameInputStartGameButton = GameObject.Find("nameInputStartGameButton").GetComponent<Button>();

        mainMenuStartGameButton.onClick.AddListener(() => GameManager.Instance.ShowPage(1));
        nameInputStartGameButton.onClick.AddListener(() => GameManager.Instance.ShowPage(2));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
