﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBehaviour : MonoBehaviour
{
    public GameObject OnNotHitEffect;
    public GameObject OnHitEffect;
    public bool canHit = false;
    public bool canDestroy = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnNotHit()
    {
        if (canDestroy)
        {
            Instantiate(OnNotHitEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        else
        {
            //  InvokeRepeating("OnNotHit", 0.1f, 0.1f);
            //  Invoke("OnNoHit");
          //  OnNotHit();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.CompareTag("Human"))
        canDestroy = false;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        canHit = false;
        canDestroy = true;
    }
    public void ChangeHitState()
    {
        canHit = true;
    }
}
