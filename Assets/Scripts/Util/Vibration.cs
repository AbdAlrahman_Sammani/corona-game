﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vibration
{
    private static int andVer;

    public static void Vibrate(int level)
    {
#if UNITY_EDITOR
        return;
#elif UNITY_IOS
        VibrateIos(level);
#elif UNITY_ANDROID
        VibrateAnd(level);
#endif
    }

    public static void Vibrate(iOSHapticFeedback.iOSFeedbackType type)
    {
        Vibrate((int)type);
    }

    private static void VibrateAnd(int level)
    {
        Native.Vibrate(level);
    }

    private static void VibrateIos(int level)
    {
        if (iOSHapticFeedback.Instance.IsSupported())
        {
            iOSHapticFeedback.Instance.Trigger((iOSHapticFeedback.iOSFeedbackType)level);
        }
        else
        {
            Handheld.Vibrate();
        }
    }

    public static bool IsHapticSupported()
    {
#if UNITY_EDITOR
        return false;
#elif UNITY_IOS
        return iOSHapticFeedback.Instance.IsSupported();
#elif UNITY_ANDROID
        if (andVer == 0)
            andVer = Native.getSDKInt();
        return andVer >= 26;
#endif
    }
}
