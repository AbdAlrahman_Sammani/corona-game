﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Menu : MonoBehaviour
{
    public bool exitAnimation;
    public abstract void OnBackPressed();
    public abstract void Show();
    public abstract void Hide();
    public abstract void Idle();
    public abstract void ShowPage(int num);

}

public abstract class Menu<T> : Menu where T : Menu<T>
{
    public static T Instance { get; private set; }
    public UnityEvent opentEvent, closeEvent, idleEvent;
    protected virtual void Awake()
    {
        Instance = (T)this;
    }
    protected virtual void OnDestroy()
    {
        Instance = null;
    }
    protected static void Open()
    {
        Debug.Log("in Menu Open");
        /* if (Instance != null) /// use this when you want to instantiate and detsroy menues
         {
             Debug.Log("In Return");
           //  return;
         }*/
        MenuManager.Instance.OpenMenu<T>();


    }
    protected static void Close()
    {

        if (Instance == null)
        {
            Debug.LogError("Not Exist To Close THis Menu");
            return;
        }
        MenuManager.Instance.CloseMenu<T>();
    }
    public override void OnBackPressed()
    {
       // Close();
    }
    public override void Show()
    {
        Open();
        opentEvent.Invoke();
        // throw new System.NotImplementedException();
    }
    public override void Hide()
    {
        Close();
        closeEvent.Invoke();
        // throw new System.NotImplementedException();
    }
    public override void Idle()
    {
        idleEvent.Invoke();
        throw new System.NotImplementedException();
    }
    public override void ShowPage(int num)
    {
        GetComponent<Animator>().SetTrigger("Close");

        StartCoroutine(CloseAfterAnimationEnd(num));

    }

    public IEnumerator CloseAfterAnimationEnd(int num)
    {
        yield return new WaitUntil(() => { return exitAnimation; });
        Debug.Log("Animatin Finished");
        switch (num)
        {
            case 0:
                MainMenu.Instance.Show();
                MainMenu.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 1:
                Page1.Instance.Show();
                Page1.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 2:
                Page2.Instance.Show();
                Page2.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 3:
                Page3.Instance.Show();
                Page3.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 4:
                Page4.Instance.Show();
                Page4.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;

            case 5:
                Page5.Instance.Show();
                Page5.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 6:
                Page6.Instance.Show();
                Page6.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 7:
                Page7.Instance.Show();
                Page7.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 8:
                Page8.Instance.Show();
                Page8.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 9:
                Page9.Instance.Show();
                Page9.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 10:
                Page10.Instance.Show();
                Page10.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            case 11:
                Page11.Instance.Show();
                Page11.Instance.GetComponent<Animator>().SetTrigger("Enter");
                break;
            default:
                break;
        }
    }
}



