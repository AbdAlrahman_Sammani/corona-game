﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Script to manage multipages app when using UISystem scripts
/// </summary>
public class MenuManager : MonoBehaviour
{

    public static MenuManager Instance { get; private set; }
    ///declare Prefabs for menus here
    ///iNSTANCE SHLOULD HAS SOMETHING LIKE THESE
    public MainMenu mainMenu;
    public Page1 page1;
    public Page2 page2;
    public Page3 page3;
    public Page4 page4;
    public Page5 page5;
    public Page6 page6;
    public Page7 page7;
    public Page8 page8;
    public Page9 page9;
    public Page10 page10;
    public Page11 page11;
    public Stack<Menu> menuStack = new Stack<Menu>();
    private void Awake()
    {
        Instance = this;
        //   mainMenu.Show();
        var x = GetMenu<MainMenu>();
        menuStack.Push(x);

    }
    public void ShowPage(int id)
    {

    }
    protected virtual void Start()
    {
        mainMenu = FindObjectOfType<MainMenu>();
        page1 = FindObjectOfType<Page1>();
        page2 = FindObjectOfType<Page2>();
        page3 = FindObjectOfType<Page3>();
        page4 = FindObjectOfType<Page4>();
        page5 = FindObjectOfType<Page5>();
        page6 = FindObjectOfType<Page6>();
        page7 = FindObjectOfType<Page7>();
        //page8 = FindObjectOfType<Page8>();
        //page9 = FindObjectOfType<Page9>();

        ///ADD SOMTHING LIKE THESE IN YOUR INSTANCE
        if (page1)
            page1.gameObject.SetActive(false);
        if (page2)
            page2.gameObject.SetActive(false);
        if (page3)
            page3.gameObject.SetActive(false);
        if (page4)
          page4.gameObject.SetActive(false);
        if (page5)
            page5.gameObject.SetActive(false);
        if (page6)
            page6.gameObject.SetActive(false);
        if (page7)
            page7.gameObject.SetActive(false);
        //if (page8)
        //    page8.gameObject.SetActive(false);
        //if (page9)
        //    page9.gameObject.SetActive(false);
        //if (page10)
        //    page10.gameObject.SetActive(false);
        //if (page11)
        //    page11.gameObject.SetActive(false);

        //  mainMenu.Show();
        mainMenu.GetComponent<Animator>().SetTrigger("Enter");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && menuStack.Count > 0)
        {
          //  menuStack.Peek().OnBackPressed();
        }
    }
    private void OnDestroy()
    {
        Instance = null;
    }

    public void OpenMenu<T>() where T : Menu
    {
        var prefab = GetMenu<T>();

        //var instance = Instantiate(prefab, transform);
        if (menuStack.Count > 0)
        {
            menuStack.Peek().gameObject.SetActive(false);
        }
        menuStack.Push(prefab);
        menuStack.Peek().gameObject.SetActive(true);
    }

    public void CloseMenu<T>() where T : Menu
    {
        var instance = menuStack.Pop();
        //  Destroy(instance.gameObject);
        instance.gameObject.SetActive(false);

        if (menuStack.Count > 0)
        {
            menuStack.Peek().gameObject.SetActive(true);
        }
    }
    protected virtual T GetMenu<T>() where T : Menu
    {
        if (typeof(T) == typeof(MainMenu) && mainMenu)
        {
            return mainMenu as T;
        }
        if (typeof(T) == typeof(Page1) && page1)
        {
            return page1 as T;
        }
        if (typeof(T) == typeof(Page2) && page2)
        {
            return page2 as T;
        }
        if (typeof(T) == typeof(Page3) && page3)
        {
            return page3 as T;
        }
        if (typeof(T) == typeof(Page4) && page4)
        {
            return page4 as T;
        }
        if (typeof(T) == typeof(Page5) && page5)
        {
            return page5 as T;
        }
        if (typeof(T) == typeof(Page6) && page6)
        {
            return page6 as T;
        }
        if (typeof(T) == typeof(Page7) && page7)
        {
            return page7 as T;
        }
        if (typeof(T) == typeof(Page8) && page8)
        {
            return page8 as T;
        }
        if (typeof(T) == typeof(Page9) && page9)
        {
            return page9 as T;
        }
        if (typeof(T) == typeof(Page10) && page10)
        {
            return page10 as T;
        }
        if (typeof(T) == typeof(Page11) && page11)
        {
            return page11 as T;
        }
        throw new MissingReferenceException();
    }
}
