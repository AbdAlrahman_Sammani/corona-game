﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class MainMenu : Menu<MainMenu>
{
    public UnityEvent onenable;
    public override void OnBackPressed()
    {
        Application.Quit();
    }
    private void Update()
    {
        exitAnimation = GetComponent<Animator>().GetNextAnimatorStateInfo(0).IsName("Default");
    }
    private void OnEnable()
    {
        onenable.Invoke();   
    }
}
