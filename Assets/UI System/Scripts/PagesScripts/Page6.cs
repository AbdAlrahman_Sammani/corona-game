﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Page6 : Menu<Page6>
{
    public TextMeshProUGUI endGameScoretext;
  //  public ScoreManager scoreManager;

    private void Update()
    {
        exitAnimation = GetComponent<Animator>().GetNextAnimatorStateInfo(0).IsName("Default");
    //    endGameScoretext.text ="Score="+ scoreManager.currentScorePlayer1.ToString();

    }
    public IEnumerator ClosePage()
    {
        yield return new WaitForSeconds(5);
        ShowPage(0);
    }
    public void StartCoro()
    {
        StartCoroutine(ClosePage());
    }
    private void OnEnable()
    {
        StartCoro();
    }

}